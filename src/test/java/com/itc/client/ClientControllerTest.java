package com.itc.client;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.event.annotation.BeforeTestExecution;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itc.client.dao.ClientDao;
import com.itc.client.model.Client;
import com.itc.client.service.ClientService;

@SpringBootTest
public class ClientControllerTest {
	
	@Autowired
    private WebApplicationContext webApplicationContext;
	public MockMvc mockMvc;
	
	@Autowired
	public ClientService ClientService;
	
	@MockBean
	public ClientDao clientDao;
	
	
	 @BeforeEach
	  public void setUp() {
	    mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	  }
	
	@Test
	public void test_save_client() throws Exception
	{
		
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(new Client(null, "sarvesh", "yg", "9980762601", "SA02", null));
		
		RequestBuilder request =  MockMvcRequestBuilders
				                  .post("/api/v1/ClientService/")
				                  .content(json) 
				                  .contentType(MediaType.APPLICATION_JSON)
				                  .accept(MediaType.APPLICATION_JSON);
			
		  MvcResult result = mockMvc.perform(request).andExpect(status().isCreated()).andReturn();
		    
	}
	
	@Test
	public void test_getClient_by_id() throws Exception
	{
		List<Client> list = Arrays.asList(new Client(null, "sarvesh", "yg", "9980762601", "SA02", null),new Client(null, "abhi", "hs", "7259580670", "SA03", null));
		
		when(clientDao.getClients()).thenReturn(list);
		
		RequestBuilder request =  MockMvcRequestBuilders
                .get("/api/v1/ClientService/clientId/SA02")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
		
		
		 MvcResult result = mockMvc.perform(request)
				                   .andDo(print()) 
				                   .andExpect(status().isOk())
				                   .andExpect(jsonPath("$.[*].first_name", hasItems("sarvesh")))
				                   .andExpect(jsonPath("$.[*].last_name", hasItems("yg")))
				                   .andExpect(jsonPath("$.[*].mobile_number", hasItems("9980762601")))
				                   .andExpect(jsonPath("$.[*].id_number", hasItems("SA02")))
				                   .andReturn();
		 	
		
	}
	
	
	@Test
	public void test_getClient_by_number() throws Exception
	{
		List<Client> list = Arrays.asList(new Client(null, "sarvesh", "yg", "9980762601", "SA02", null),new Client(null, "abhi", "hs", "7259580670", "SA03", null));
		
		when(clientDao.getClients()).thenReturn(list);
		
		RequestBuilder request =  MockMvcRequestBuilders
                .get("/api/v1/ClientService/clientNumber/7259580670")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
		
		
		 MvcResult result = mockMvc.perform(request)
				                   .andDo(print()) 
				                   .andExpect(status().isOk())
				                   .andExpect(jsonPath("$.[*].first_name", hasItems("abhi")))
				                   .andExpect(jsonPath("$.[*].last_name", hasItems("hs")))
				                   .andExpect(jsonPath("$.[*].mobile_number", hasItems("7259580670")))
				                   .andExpect(jsonPath("$.[*].id_number", hasItems("SA03")))
				                   .andReturn();
		 	
		
	}
	
	@Test
	public void test_dup_client_by_id() throws Exception
	{
		
		List<Client> list = Arrays.asList(new Client(null, "sarvesh", "yg", "9980762601", "SA02", null),new Client(null, "abhi", "hs", "7259580670", "SA03", null));
		
		when(clientDao.getClients()).thenReturn(list);
		
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(new Client(null, "atul", "telkar", "9980762601", "SA02", null));
		
		RequestBuilder request =  MockMvcRequestBuilders
				                  .post("/api/v1/ClientService/")
				                  .content(json) 
				                  .contentType(MediaType.APPLICATION_JSON)
				                  .accept(MediaType.APPLICATION_JSON);
			
		  MvcResult result = mockMvc.perform(request).andExpect(status().isBadRequest()).andReturn();
		    
	}
	
	@Test
	public void test_dup_client_by_number() throws Exception
	{
		
		List<Client> list = Arrays.asList(new Client(null, "sarvesh", "yg", "9980762601", "SA02", null),new Client(null, "abhi", "hs", "9981", "SA03", null));
		
		when(clientDao.getClients()).thenReturn(list);
		
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(new Client(null, "Nag", "telkar", "9980762601", "SA04", null));
		
		RequestBuilder request =  MockMvcRequestBuilders
				                  .post("/api/v1/ClientService/")
				                  .content(json) 
				                  .contentType(MediaType.APPLICATION_JSON)
				                  .accept(MediaType.APPLICATION_JSON);
			
		  MvcResult result = mockMvc.perform(request).andExpect(status().isBadRequest()).andReturn();
		    
	}

	
	
}
