package com.itc.client.dao;

import java.util.ArrayList;
import java.util.List;import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.itc.client.model.Client;
import org.springframework.stereotype.Service;

@Service
public class ClientDao {
	
	private static List<Client>  clientList = new ArrayList();
	
	
	public Client saveClient(Client client)
	{
		clientList.add(client);
		return client;
		
	}
	
	public List<Client> getClients()
	{
		return clientList;
	}
	
	
}
