package com.itc.client.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itc.client.model.Client;
import com.itc.client.service.ClientService;

@RestController
@RequestMapping(value = "/api/v1/ClientService/")
@Validated
public class ClientController {
	
	
	@Autowired
	private ClientService ClientService;

	@PostMapping
	public ResponseEntity<Client> saveClient( @Valid @RequestBody Client client) {
		ClientService.saveClient(client);
		
		return new ResponseEntity<Client>(HttpStatus.CREATED);
	}

	@GetMapping("clientId/{id}")
	public ResponseEntity<List<Client>> getClientByIdNmbr(@PathVariable String id) {
		List<Client> client = ClientService.getClientByIdNumber(id);
		return new ResponseEntity<List<Client>>(client, HttpStatus.OK);
	}
	
	@GetMapping("clientNumber/{number}")
	public ResponseEntity<List<Client>> getClientByMobNum(@PathVariable String number) {
		List<Client> client = ClientService.getClientByMobileNumber(number);
		return new ResponseEntity<List<Client>>(client, HttpStatus.OK);
	}
	
	@GetMapping("clientName/{name}")
	public ResponseEntity<List<Client>> getClientByName(@PathVariable String name) {
		List<Client> client = ClientService.getClientByFirstName(name);
		return new ResponseEntity<List<Client>>(client, HttpStatus.OK);
	}
	
}
