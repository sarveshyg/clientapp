package com.itc.client.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorResponse {

	private String message;
	private HttpStatus statuscode;
	private Date timestamp;

}
