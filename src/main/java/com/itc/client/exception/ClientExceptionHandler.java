package com.itc.client.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ClientExceptionHandler {
	
	@ExceptionHandler(ClientDuplicateByIdException.class)
	public ResponseEntity<ErrorResponse> handleClientDuplicateByIdException(ClientDuplicateByIdException exception) {

		ErrorResponse errorResponse = ErrorResponse.builder().message(exception.getMessage())
				.statuscode(HttpStatus.BAD_REQUEST).timestamp(new Date()).build();

		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.BAD_REQUEST);

	}
	@ExceptionHandler(ClientDuplicateByNumberException.class)
	public ResponseEntity<ErrorResponse> handleClientDuplicateByNumberException(ClientDuplicateByNumberException exception) {

		ErrorResponse errorResponse = ErrorResponse.builder().message(exception.getMessage())
				.statuscode(HttpStatus.BAD_REQUEST).timestamp(new Date()).build();

		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.BAD_REQUEST);

	}

}
