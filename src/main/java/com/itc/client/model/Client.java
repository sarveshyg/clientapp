package com.itc.client.model;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Client {
	
	@JsonProperty("id")
	private Integer id;
	
	@NotBlank(message = "name can't be empty")
	@Size(min=3,max = 20,message = "name must be 3 and 20 characters")
	@Pattern(regexp = "^[a-zA-Z]+$",message = "name should only contains letters")
	@JsonProperty("first_name")
	private String firstName;
	
	@NotBlank(message = "name can't be empty")
	@Size(min=2,max = 20,message = "name must be 3 and 20 characters")
	@Pattern(regexp = "^[a-zA-Z]+$",message = "name should only contains letters")
	@JsonProperty("last_name")
	private String lastName;
	
	@Pattern(regexp = "\\d+",message = "mobile number should only contains letters digits")
	@Size(min=10,max = 10,message = "mobile number must be 10 characters")
	@JsonProperty("mobile_number")
	private String mobileNumber;
	
	@NotBlank(message = "Id number can't be empty")
	@Size(min=3,max = 20,message = "Id number must be 6 and 10 characters")
	@Pattern(regexp = "^[A-Z0-9]+$",message = "Id number should only contains letters and digits")
	@JsonProperty("id_number")
	private String idNumber;
	
	@Size(min=20,max = 30,message = "address must be 20 and 30 characters")
	@JsonProperty("physical_address")
	private String physicalAddress;
	

}
