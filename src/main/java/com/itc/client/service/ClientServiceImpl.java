package com.itc.client.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itc.client.dao.ClientDao;
import com.itc.client.exception.ClientDuplicateByIdException;
import com.itc.client.exception.ClientDuplicateByNumberException;
import com.itc.client.model.Client;

@Service
public class ClientServiceImpl  implements ClientService  {
	
	@Autowired
	ClientDao clientDao;
	

	public Client saveClient(Client client)  {
		
		
		List<Client> clientList = clientDao.getClients();
		
		
		List<Client> list = clientList
		             .stream()
		             .filter(client1 -> client1.getIdNumber().equals(client.getIdNumber()))
		             .collect(Collectors.toList());
		
		if(!list.isEmpty())
		{
			throw new ClientDuplicateByIdException("client id alredy exist");
		}
		
		 list = clientList
	             .stream()
	             .filter(client1 -> client1.getMobileNumber().equals(client.getMobileNumber()))
	             .collect(Collectors.toList());
		 
			if(!list.isEmpty())
			{
				throw new ClientDuplicateByNumberException("client mobile number already exist");
			}
			
			return clientDao.saveClient(client);
		
	}

	public List<Client> getClientByFirstName(String name) {
		 
		List<Client> clientList = clientDao.getClients();
		
		return clientList
				.stream()
				.filter(client -> client.getFirstName().equals(name))
				.collect(Collectors.toList());
	}

	public List<Client> getClientByIdNumber(String id) {
		
		List<Client> clientList = clientDao.getClients();
		
		return clientList
				.stream()
				.filter(client -> client.getIdNumber().equals(id))
				.collect(Collectors.toList());
		
	}

	public List<Client> getClientByMobileNumber(String number) {
		
		List<Client> clientList = clientDao.getClients();
		return clientList
				.stream()
				.filter(client -> client.getMobileNumber().equals(number))
				.collect(Collectors.toList());
	}
	
	
}
