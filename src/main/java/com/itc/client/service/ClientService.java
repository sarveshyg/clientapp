package com.itc.client.service;

import java.util.List;

import com.itc.client.model.Client;

public interface ClientService {
	
	Client saveClient(Client client);
	
    List<Client> getClientByFirstName(String name);
    
    List<Client> getClientByIdNumber(String id);
    
    List<Client> getClientByMobileNumber(String number);

}
